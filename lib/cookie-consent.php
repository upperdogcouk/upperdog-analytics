<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class UDAnalyticsCookieConsent {


    /**
     * CMS option fields and with default values
     */
    private $fields = [
        'cookie_popup_title' => 'This site uses cookies',
        'cookie_popup_paragraph' => 'Some of these cookies are essential, while others help us to improve your experience by providing insights into how the site is being used.',
        'cookie_toggle_colour' => '2196f3',
        'cookie_title_colour' => 'ffffff',
        'understand_button_text' => 'I understand',
        'cookie_background_colour' => 'dark',
        'necessary_cookie_info' => 'Necessary cookies enable core functionality. The website cannot function properly without these cookies, and can only be disabled by changing your browser preferences.',
        'analytical_cookie_info' => 'These cookies help us to improve our website by providing insights into how the site is being used.',
        'analytical_cookie_scripts' => <<<EOD
gtag('consent', 'update', {
   'analytics_storage': 'granted',
});

EOD,
        'marketing_cookie_info' => 'These cookies allow us to understand how you interact with our website so we can serve relevant ads to you based on your browsing behaviour.',
        'marketing_cookie_scripts' => <<<EOD
gtag('consent', 'update', {
    'ad_storage': 'granted',
    'personalization_storage' : 'granted',
    'functionality_storage' : 'granted',
    'ad_personalization': 'granted',
    'ad_user_data': 'granted',
});
EOD,
    ];


    /**
     * The single instance of the class.
     */
    protected static $_instance = null;


    /**
     * Ensures only one instance of `UDAnalytics` is loaded or can be loaded.
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    /**
     * On class initialization
     */
    public function init()
    {

        // load option settings into class
        $this->load_option_settings();

        // shortcode for open cookie settings
        add_shortcode('config_ud_cookie_consent_button', [$this, 'cookie_consent_config_button']);

        // on plugin install setup default field in wp options
        add_action('upperdog_analytics_plugin_install', [$this, 'setup_fields']);

        // process save on csm options page
        add_action('upperdog_analytics_process_tab_cookies', [$this, 'process_tab_cookies']);

        // js for cookie consent content
        add_action('upperdog_analytics_output_ud_analytics_js', [$this, 'insert_cookie_consent_html']);

        // js for analytical cookies
        add_action('upperdog_analytics_output_ud_analytics_js', [$this, 'insert_analytical_cookies_js']);

        // js for marketing cookies
        add_action('upperdog_analytics_output_ud_analytics_js', [$this, 'insert_marketing_cookies_js']);

        // check to see whether to load js to footer
        add_action( 'wp_footer', [$this, 'load_js']);

    }


    /**
     *  Shortcode for open cookie settings
     */
    function cookie_consent_config_button() {
        echo '<button onclick="window.click_cookie_consent_config_button();">
                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 120 123">
                    <path fill-rule="evenodd" d="M98 0a6 6 0 1 1 0 12 6 6 0 0 1 0-12zm2 52c5 8 11 10 20 5v9A60 60 0 1 1 0 60C2 27 31 1 64 2c-3 10 0 18 7 21-7 21 8 36 29 29zm-70-4a9 9 0 1 1 0 18 9 9 0 0 1 0-18zm28 11a5 5 0 1 1 0 11 5 5 0 0 1 0-11zM36 81a6 6 0 1 1 0 12 6 6 0 0 1 0-12zm13-49a5 5 0 1 1 0 10 5 5 0 0 1 0-10zm27 51a8 8 0 1 1 0 16 8 8 0 0 1 0-16zm18-60a6 6 0 1 1 0 11 6 6 0 0 1 0-11z" clip-rule="evenodd"/>
                </svg>
            </button>';

    }


    /**
     * Load option settings into class
     */
    function load_option_settings() {
        if ($upperdog_analytics_cookie_consent = get_option("upperdog_analytics_cookie_consent")) {

            // loop through fields and add filters
            foreach ($upperdog_analytics_cookie_consent as $key => $value) {
                $upperdog_analytics_cookie_consent[$key] = apply_filters('uda_field_' . $key, $value);
            }
            $this->fields = $upperdog_analytics_cookie_consent;

        }
    }


    /**
     * Detects if user has accepted analytical cookies
     */
    function has_accepted_analytical_cookies() {
        return (isset($_COOKIE["ud_accept_analytical_cookies"]) && $_COOKIE["ud_accept_analytical_cookies"]);
    }


    /**
     * Detects if user has accepted marketing cookies
     */
    function has_accepted_marketing_cookies() {
        return (isset($_COOKIE["ud_accept_marketing_cookies"]) && $_COOKIE["ud_accept_marketing_cookies"]);
    }


    /**
     * Detects if user has acknowledged analytical cookie options
     */
    function has_set_analytical_cookies() {
        return (isset($_COOKIE["ud_accept_analytical_cookies"]));
    }


    /**
     * Detects if user has acknowledged marketing cookie options
     */
    function has_set_marketing_cookies() {
        return (isset($_COOKIE["ud_accept_marketing_cookies"]));
    }


    /**
     * On plugin install setup default field in wp options
     */
    public function setup_fields()
    {
        if (!get_option("upperdog_analytics_cookie_consent")) {
            update_option("upperdog_analytics_cookie_consent", $this->fields);
        }
    }


    /**
     * Process save on csm options page
     */
    public function process_tab_cookies()
    {
        if (!isset($_POST['save'])) {
            return;
        }
        $fields = [];
        foreach ( $this->fields as $field_k => $field) {
            $fields[$field_k] = isset($_POST[$field_k]) ? $_POST[$field_k] : $field;
        }
        update_option("upperdog_analytics_cookie_consent", $fields);

        add_action( 'upperdog_analytics_process_tab_response', function () { ?>
        <div class="notice notice-success is-dismissible">
            <p>Cookie settings saved</p>
        </div>
        <?php
        });

    }


    /**
     * Reads a file into string
     */
    function get_file($file) {
        $path = WP_PLUGIN_DIR . '/upperdog-analytics/' . $file;
        if (file_exists($path)) {
            ob_start();
            include($path);
            $content = ob_get_clean();
            $content = addslashes(str_replace(["\n", "\r"], '', $content));
            return $content;
        }
    }


    /**
     * Returns bool to whether cookie consent is required
     */
    public function is_cookie_consent_not_required()
    {
        return  (!$this->show_analytical_cookies_toggler() && !$this->show_marketing_cookies_toggler());
    }


    /**
     * Detect if is a bot
     */
    public function is_a_bot()
    {
        $is_bot = false;
        $user_agents = ['GTmetrix', 'Googlebot', 'Chrome-Lighthouse', 'Bingbot', 'BingPreview', 'msnbot', 'slurp', 'Ask Jeeves/Teoma', 'Baidu', 'DuckDuckBot', 'AOLBuild'];
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        foreach ($user_agents as $agent){
            if (strpos($user_agent, $agent)){
                $is_bot = true;
            }
        }
        return $is_bot;
    }


    /**
     * Js for cookie consent content
     */
    public function insert_cookie_consent_html()
    {

        // if cookie popup is disabled don't proceed
        if ($this->disable_cookie_popup()) {
            return;
        }

        // if is a bot
        if ($this->is_a_bot()) {
            return;
        }

        $html = '';

        // output cookie consent js
        echo 'var ccjs = document.createElement(\'script\'); ccjs.type = \'text/javascript\'; ccjs.async = true; ccjs.src = \'' .  UDAnalytics()->uri('assets/js/cookie-popup.js') . '\'; var udaccs = document.getElementsByTagName(\'script\')[0]; udaccs.parentNode.insertBefore(ccjs, udaccs);';

        // add cookie consent css
        $html .= '<link rel=\"stylesheet\" href=\"' . UDAnalytics()->uri('assets/css/cookie-popup.css') . '\">';

        // add cookie consent html
        $html .= $this->get_file('templates/cookie-consent-popup.php');

        // if preference has been set then hide popup
        if ($this->has_set_analytical_cookies() && $this->has_set_marketing_cookies()) {
            $html = str_replace('class=\"cpu-wrapper\"', 'class=\"cpu-wrapper pop-up-hidden\"', $html);
            echo "document.body.classList.remove('cpu-open');";
        } else {
            echo "document.body.classList.add('cpu-open');";
        }

        // insert end of body
        echo 'document.body.insertAdjacentHTML("beforeend", "' . $html . '");';

    }


    /**
     * Custom analytical cookies js
     */
    public function insert_analytical_cookies_js()
    {
        $js = stripslashes($this->fields['analytical_cookie_scripts']);
        if ('!' == $js) {
            $js = null;
        }
        echo ' document.body.addEventListener("do_ud_analytical_cookies_js", () => { ' . $js . ' });';
    }


    /**
     * Custom marketing cookies js
     */
    public function insert_marketing_cookies_js()
    {
        $js = stripslashes($this->fields['marketing_cookie_scripts']);
        if ('!' == $js) {
            $js = null;
        }
        echo ' document.body.addEventListener("do_ud_marketing_cookies_js", () => { ' . $js . ' });';
    }


    /**
     * Get js to output
     */
    public function get_js($key)
    {
        $js = stripslashes($this->fields[$key]);
        if ('!' == $js) {
            $js = null;
        }
        return $js;
    }


    /**
     * Initial inline js
     */
    public function load_js()
    {
        if ($this->has_accepted_analytical_cookies() || $this->has_accepted_marketing_cookies()) {
            $js = '<script>';
            if ($this->has_accepted_analytical_cookies()) {
                $js .= $this->get_js('analytical_cookie_scripts');
            }
            if ($this->has_accepted_marketing_cookies()) {
                $js .= $this->get_js('marketing_cookie_scripts');
            }
            $js .= '</script>';
            echo $js;
        }
    }


    /**
     * Gets a field option
     */
    public function get_field($field)
    {
        if (isset($this->fields[$field])) {
            return $this->fields[$field];
        }
    }


    /**
     * Show analytical cookies toggler
     */
    public function show_analytical_cookies_toggler()
    {
        $val = $this->get_field('analytical_cookie_scripts');
        if ('!' == $val) {
            $val = null;
        }
        return ($val);
    }


    /**
     * Detect if cookie popup should be disabled
     */
    public function disable_cookie_popup()
    {
        return empty($this->get_field('cookie_popup_title'));
    }


    /**
     * Show marketing cookies toggler
     */
    public function show_marketing_cookies_toggler()
    {
        $val = $this->get_field('marketing_cookie_scripts');
        if ('!' == $val) {
            $val = null;
        }
        return ($val);
    }


}



/**
 * Public function
 */
function UDAnalyticsCookieConsent() {
    return UDAnalyticsCookieConsent::instance();
}


/**
 * Initializing functions & listeners
 */
UDAnalyticsCookieConsent()->init();
