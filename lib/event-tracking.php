<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class UDEventTracking {

    /**
     * The single instance of the class.
     */
    protected static $_instance = null;


    /**
     * Ensures only one instance of `UDEventTracking` is loaded or can be loaded.
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    /**
     * Class initialization
     */
    public function init()
    {

        // hook listeners
        $this->listeners();

    }


    /**
     * Listen for event hooks
     */
    function listeners() {

        // process and save the analytics rules
        add_action('upperdog_analytics_process_tab_ud-analytics', [$this, 'process_save_analytics_rules']);

        // sets up the default tracking fields when the plugin gets activated
        add_action('upperdog_analytics_plugin_install', [$this, 'setup_fields']);

        // inserts the tracking js into `ud-analytics.js`
        add_action('upperdog_analytics_output_ud_analytics_js', [$this, 'insert_event_tracking_js']);

    }


    /**
     * Process and save the analytics rules
     */
    function process_save_analytics_rules() {

        // exit if no save var passed
        if (!isset($_POST['save'])) {
            return;
        }

        // variable to store rule data
        $rule_data = [];

        // capture rule data in post
        if (isset($_POST['rule_i'])) {
            foreach ($_POST['rule_i'] as $i => $rule_i) {
                $rule_data[$i] = [
                    'selector'  =>  $_POST['selector'][$i],
                    'action'  =>  $_POST['action'][$i],
                    'event_category'  =>  $_POST['event_category'][$i],
                    'event_action'  =>  $_POST['event_action'][$i],
                    'event_label'  =>  $_POST['event_label'][$i],
                ];
            }

            // update option
            update_option("upperdog_analytics_event_tracking_rules", $rule_data);
            
        }

    }


    /**
     * Get the rules from the wp options table
     */
    function get_rules() {
        return get_option("upperdog_analytics_event_tracking_rules");
    }


    /**
     * Sets up the default tracking fields when the plugin gets activated
     */
    function setup_fields() {
        if (!get_option("upperdog_analytics_event_tracking_rules")) {
            update_option("upperdog_analytics_event_tracking_rules", [
                [
                    'selector'  =>  'a[href^=\'tel:\']',
                    'action'  =>  'click',
                    'event_category'  =>  'Phone Call Tracking',
                    'event_action'  =>  'Click to call',
                    'event_label'  =>  '',
                ],
                [
                    'selector'  =>  'a[href^=\'mailto:\']',
                    'action'  =>  'click',
                    'event_category'  =>  'Email Tracking',
                    'event_action'  =>  'Click to email',
                    'event_label'  =>  '',
                ]
            ]);
        }
    }


    /**
     * Inserts the tracking js into `ud-analytics.js`
     */
    function insert_event_tracking_js() {

        if ($rules = UDEventTracking()->get_rules()) {
            echo "\r\n";
            foreach ($rules as $rule_i => $rule) {
                echo '
document.querySelectorAll("' . stripslashes($rule['selector']) . '").forEach(selector' . $rule_i . ' => 
  selector' . $rule_i . '.addEventListener("click",() => 
  {
        window.gtag(\'event\', \'' . stripslashes($rule['action']) . '\', {
            \'event_category\': \'' . stripslashes($rule['event_category']) . '\',
            \'event_action\': \'' . stripslashes($rule['event_action']) . '\',
            \'event_label\': \'' . stripslashes($rule['event_label']) . '\'
        });
  })
);';
            }
        }

    }


}


/**
 * Public function instantiating wish list class
 */
function UDEventTracking() {
    return UDEventTracking::instance();
}


/**
 * Initializing functions & listeners
 */
UDEventTracking()->init();