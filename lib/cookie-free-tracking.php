<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class UDAnalyticsCookieFreeGA {


    /**
     * On class construction
     */
    public function __construct()
    {

        // insert cookie free js code to header
        add_action('wp_head', [$this, 'output_cookie_free_ga_js']);

        add_action('get_header', [$this, 'output_cookie_free_ga_body'], -100);

    }


    /**
     * Insert cookie free js code to header
     */
    public function output_cookie_free_ga_js($order)
    {
        if ($gtm_code = UDAnalytics()->get_gtm_code()) :
        ?>

            <script>
                // Define dataLayer and the gtag function.
                window.dataLayer = window.dataLayer || [];
                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('consent', 'default', {
                    'ad_storage': 'denied',
                    'ad_personalization': 'denied',
                    'ad_user_data': 'denied',
                    'analytics_storage': 'denied',
                    'functionality_storage': 'denied',
                    'personalization_storage': 'denied',
                    'security_storage': 'granted',
                    'wait_for_update': 2000,
                });

                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','<?=$gtm_code?>');
            </script>

        <?php
        endif;
    }

    public function output_cookie_free_ga_body()
    {
        if ($gtm_code = UDAnalytics()->get_gtm_code()) :
            ?>

            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?=$gtm_code?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->

        <?php
        endif;
    }

}
new UDAnalyticsCookieFreeGA();