<?php
if (!defined('ABSPATH')) {
    exit;
}

class UDAnalyticsEcommerceTracking {

    /**
     * Instance of the class.
     */
    protected static $_instance = null;


    /**
     * Ensures only one instance of `ms_wc_multiple_addresses` is loaded or can be loaded.
     */
    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    /**
     * Initialization function
     */
    public function init()
    {
        add_action('init', [$this, '_init']);
    }


    /**
     * Functions to run in initialization
     */
    public function _init()
    {

        // if the ecommerce tracking not enabled then exit
        if (!$this->is_ecommerce_tracking_enabled()) {
            return;
        }

        // static page event for `single product` page. trigger ud-event `view_item`
        add_action('wp_footer', [$this, 'trigger_js_ud_event_view_item_on_single_product']);

        // static page event for `basket` page. trigger ud-event `view_cart`
        add_action('wp_footer', [$this, 'trigger_js_ud_event_view_cart_on_basket']);

        // static page event for `checkout` page. trigger ud-event `begin_checkout`
        add_action('wp_footer', [$this, 'trigger_js_ud_event_begin_checkout_on_checkout']);

        // static page event `add to cart` trigger ud-event `add_to_cart` when standard wp add to cart happens
        add_action('woocommerce_add_to_cart', [$this, 'trigger_js_ud_event_add_to_cart_on_woocommerce_add_to_cart'], 10, 6);

        // static page `product listing` trigger ud-event `view_item_list` // keep this commented, maybe we can have an area where the events can be enabled/disabled in plugin panel
        // add_action('woocommerce_after_shop_loop', [$this, 'trigger_js_ud_event_view_item_list_on_woocommerce_product_listing']);

        // ajax event `open basket`. add ecommerce variables
        add_filter('ud_slide_out_response_args_basket', [$this, 'ud_slide_out_response_args_basket_with_tracking']);

        // ajax event `add to cart`. add ecommerce variables
        add_filter('ud_shop_basket_add_to_cart_response_vars', [$this, 'ud_shop_basket_add_to_cart_response_vars_with_tracking'], 10, 3);

        // ajax event `remove from cart`. add ecommerce variables
        add_filter('ud_shop_basket_remove_from_cart_response_vars', [$this, 'ud_shop_basket_remove_from_cart_response_vars_with_tracking'], 10, 3);

        // ajax event `update cart quantity`. add ecommerce variables
        add_filter('ud_shop_basket_update_cart_qty_response_vars', [$this, 'ud_shop_basket_update_cart_qty_response_vars_with_tracking'], 10, 3);

        // ajax event `shop_feed`. add ecommerce variables
        add_filter('ud_shop_ajax_output', [$this, 'ud_shop_ajax_output_response_vars_with_tracking'], 10, 3);

        // static page event for `woocommerce thankyou` page. trigger ud-event `purchase`
        add_action('woocommerce_thankyou', [$this, 'trigger_js_ud_event_purchase_on_checkout_thankyou']);

        // add gtag ecommerce event listeners
        add_action('upperdog_analytics_output_ud_analytics_js', [$this, 'setup_gtag_ecommerce_js']);

        // clear gtag ecommerce array before view cart event (introduce later)
        // add_filter('upperdog_analytics_ecommerce_event_js_view_cart', [$this, 'event_js_view_cart_clear_ecommerce_array'], 10, 3);
    }


    /**
     * Detect whether ecommerce tracking is enabled
     */
    public function is_ecommerce_tracking_enabled()
    {
        if (!$this->is_woocommerce_activated()) {
            return;
        }
        return UDAnalytics()->is_tracking_ecommerce();
    }


    /**
     * Returns the proper class based on Gtag settings.
     */
    protected function get_tracking_instance($options = [])
    {
        return WC_Google_Gtag_JS::get_instance($options);
    }


    /**
     * Check if WooCommerce is activated
     */
    public function is_woocommerce_activated()
    {
        return is_plugin_active('woocommerce/woocommerce.php');
    }


    /**
     * Additional meta data field keys
     */
    public function add_additional_inventory_meta_field_keys() {
        return UDAnalytics()->get_tracking_product_meta_keys();
    }


    /**
     * Function that outputs js `ud-event` code.
     */
    public function ud_event_js($event_name, $data = false) {
        return "jQuery(document.body).trigger('ud-event', ['$event_name', " . json_encode($data) . "]);";
    }


    /**
     * Output inline js script which will output a `view_item` gta event on a single product page.
     */
    public function ud_event_output_js_script($event_name, $data = false, $on_ready = true) {
        $js = '<script>';
        if ($on_ready) {
            $js .= "jQuery(document.body).on('ud_ecommerce_tracking_started', () => { ";
        }
        $js .= apply_filters('upperdog_analytics_ecommerce_event_js_' . $event_name, $this->ud_event_js($event_name, $data), $data);
        if ($on_ready) {
            $js .= "});";
        }
        $js .= '</script>';
        echo $js;
    }


    /**
     * Output inline js script which will output a `view_item` gta event on a single product page.
     */
    public function trigger_js_ud_event_view_item_on_single_product() {
        if (!is_product()) {
            return;
        }
        global $product;
        $items = [$this->generate_event_item($product)];
        $items_value = $this->get_event_items_value($items);
        $data = [
            'vars' => [
                'ecommerce' => [
                    'items' =>  $items,
                    'value' =>  $items_value,
                ]
            ]
        ];

        $this->ud_event_output_js_script('view_item', $data);

    }


    /**
     * Output inline js script which will output a `view_cart` gta event on the basket page.
     */
    public function trigger_js_ud_event_view_cart_on_basket() {
        if (!is_cart()) {
            return;
        }

        // build items array
        $items = [];

        // get cart item product id
        if ($cart_items = WC()->cart->get_cart()) {
            foreach ($cart_items as $cart_item) {
                $items[] = $this->generate_event_item($cart_item['data']->get_id(), $cart_item['quantity']);
            }
        }

        // build ecommerce array
        $ecommerce = [
            'items' => $items,
            'value' => $this->get_event_items_value($items),
        ];

        // add ecommerce vars to data
        $data = [
            'vars' => [
                'ecommerce' => $ecommerce
            ]
        ];

        // output js event script
        $this->ud_event_output_js_script('view_cart', $data);
    }


    /**
     * Output inline js script which will output a `begin_checkout` gta event at start of checkout.
     */
    public function trigger_js_ud_event_begin_checkout_on_checkout() {

        // detect if we are on checkout start
        $is_checkout_start = is_checkout() && ! (is_wc_endpoint_url('order-pay') || is_wc_endpoint_url('order-received'));

        // if not on checkout start exit
        if (!$is_checkout_start) {
            return;
        }

        // build items array
        $items = [];

        // get cart item product id
        if ($cart_items = WC()->cart->get_cart()) {
            foreach ($cart_items as $cart_item) {
                $items[] = $this->generate_event_item($cart_item['data']->get_id(), $cart_item['quantity']);
            }
        }

        // build ecommerce array
        $ecommerce = [
            'items' => $items,
            'value' => $this->get_event_items_value($items),
        ];

        // add ecommerce vars to data
        $data = [
            'vars' => [
                'ecommerce' => $ecommerce
            ]
        ];

        // output js event script
        $this->ud_event_output_js_script('begin_checkout', $data);
    }


    /**
     * Output js script with add to cart gta event.
     */
    public function trigger_js_ud_event_add_to_cart_on_woocommerce_add_to_cart($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data) {
        $product_id = $variation_id ? $variation_id : $product_id;
        add_action('wp_footer', function () use ($product_id, $quantity) {
            $items = [$this->generate_event_item($product_id, $quantity)];
            $items_value = $this->get_event_items_value($items);
            $data = [
                'vars' => [
                    'ecommerce' => [
                        'items' =>  $items,
                        'value' =>  $items_value,
                    ]
                ]
            ];
            $this->ud_event_output_js_script('add_to_cart', $data);
        });
    }


//    /**
//     * View items list static page version // keep this commented, maybe we can have an area where the events can be enabled/disabled in plugin panel
//     */
//    public function trigger_js_ud_event_view_item_list_on_woocommerce_product_listing() {
//        add_action('wp_footer', function () {
//            $items = [$this->generate_event_item($product_id, $quantity)];
//            $items_value = $this->get_event_items_value($items);
//            $data = [
//                'vars' => [
//                    'ecommerce' => [
//                        'items' =>  $items,
//                        'value' =>  $items_value,
//                    ]
//                ]
//            ];
//            $this->ud_event_output_js_script('view_item_list', $data);
//        });
//    }


    /**
     * Get ecommerce data with cart items and costs
     */
    public function get_cart_ecommerce_data() {

        // build items array
        $items = [];

        // get cart item product id
        if ($cart_items = WC()->cart->get_cart()) {
            foreach ($cart_items as $cart_item) {
                $items[] = $this->generate_event_item($cart_item['data']->get_id(), $cart_item['quantity']);
            }
        }

        // build ecommerce array
        return [
            'items' => $items,
            'value' => $this->get_event_items_value($items),
        ];
    }


    /**
     * When slide out basket opens add tracking data in relation to the basket
     */
    public function ud_slide_out_response_args_basket_with_tracking($response_args) {

        // add ecommerce vars to basket slide out args
        $response_args['ecommerce'] = $this->get_cart_ecommerce_data();

        // return updated args
        return $response_args;
    }


    /**
     * Remove item from cart ajax, attach ecommerce tracking variables.
     */
    public function ud_shop_basket_add_to_cart_response_vars_with_tracking($vars, $product_id, $quantity) {
        $items = [$this->generate_event_item($product_id, $quantity)];
        $ecommerce = [
            'items' => $items,
            'value' => $this->get_event_items_value($items),
        ];
        $vars['ecommerce'] = $ecommerce;
        return $vars;
    }


    /**
     * Remove item from cart ajax, attach ecommerce tracking variables.
     */
    public function ud_shop_basket_remove_from_cart_response_vars_with_tracking($vars, $cart_item_key) {
        // build items array
        $items = [];

        // get cart item product id
        if ($cart_item = WC()->cart->get_cart_item($cart_item_key)) {
            $items = [$this->generate_event_item($cart_item['data']->get_id(), $cart_item['quantity'])];
        }

        // build ecommerce array
        $ecommerce = [
            'items' => $items,
            'value' => $this->get_event_items_value($items),
        ];

        $vars['ecommerce'] = $ecommerce;
        return $vars;
    }


    /**
     * Update cart quantity ajax, attach ecommerce tracking variables.
     */
    public function ud_shop_basket_update_cart_qty_response_vars_with_tracking($vars, $cart_items) {

        // build items array
        $items = [];

        // build a list of updated cart items
        if (!empty($cart_items)) {
            foreach ($cart_items as $cart_item_key => $quantity) {
                if ($cart_item = WC()->cart->get_cart_item($cart_item_key)) {
                    $items[] = $this->generate_event_item($cart_item['data']->get_id(), $quantity);
                }
            }
        }

        // build ecommerce array
        $ecommerce = [
            'items' => $items,
            'value' => $this->get_event_items_value($items),
        ];

        $vars['ecommerce'] = $ecommerce;
        return $vars;
    }


    /**
     * Product listing ajax feed, attach ecommerce tracking variables.
     */
    public function ud_shop_ajax_output_response_vars_with_tracking($response) {

        // build items array
        $items = [];

        // build a list of updated cart items
        if (!empty($response['items'])) {
            foreach ($response['items'] as $d) {
                $items[] = $this->generate_event_item($d['id']);
            }
        }

        // build ecommerce array
        $ecommerce = [
            'items' => $items,
            'value' => $this->get_event_items_value($items),
        ];

        // add ecommerce vars to the response
        $response['vars']['ecommerce'] = $ecommerce;

        // return updated response
        return $response;
    }


    /**
     * Purchase ga event on checkout thank you page
     *
     * Questions:
     *  - how should it report if the product is discounted
     *  - in the ecommerce items list should they be pulling through the actual cart price
     */
    public function trigger_js_ud_event_purchase_on_checkout_thankyou($order_id) {

        // don't run again if already run the action
        if (did_action('after_js_ud_event_purchase_on_checkout')) {
            return;
        }

        // get the order object
        $order = wc_get_order($order_id);

        // get the order total
        $order_total = $order->get_total();

        // get shipping total
        $shipping = number_format($order->get_shipping_total(), 2, '.', '');

        // get the currency code
        $currency = $order->get_currency();

        // get the applied coupons
        $applied_coupons = implode(', ', $order->get_coupon_codes());

        // get the total tax amount
        $tax_total = $order->get_total_tax();

        // get phone number
        $phone_number = $order->get_billing_phone();

        // get email address
        $email = $order->get_billing_email();

        // build a list of items
        $items = [];
        if (count($order->get_items()) > 0) {
            foreach ($order->get_items() as $item_id => $item) {

                // get the product id or variation id from the order item
                $product_id = $item->get_variation_id() ? $item->get_variation_id() : $item->get_product_id();

                // add item in event format
                $items[] = $this->generate_event_item($product_id, $item->get_quantity());
            }
        }

        // build final data
        $data = [
            'vars' => [
                'ecommerce' => [
                    'items' =>  $items,
                    'transaction_id' => $order_id,
                    'value' => $order_total,
                    'shipping' => $shipping,
                    'currency' => $currency,
                    'coupon' => $applied_coupons,
                    'tax' => $tax_total,
                    'phone_number' => $phone_number,
                    'email' => $email,
                ]
            ]
        ];

        // output ud event
        $this->ud_event_output_js_script('purchase', $data);

        // run action after js
        do_action('after_js_ud_event_purchase_on_checkout');
    }


    /**
     * Generate consistent event item (product) data
     */
    function generate_event_item($product, $quantity = false) {

        // get product object
        $product = is_int($product) ? wc_get_product($product) : $product;
        
        // if not a valid object then return false
        if (!is_object($product)) {
            return;
        }

        // get current category
        $current_cat = strip_tags(wc_get_product_category_list($product->get_parent_id() ? $product->get_parent_id() : $product->get_id()));

        // set up an items array
        $item = [];

        // if valid product and has meta fields
        if ($product && $meta_field_keys = $this->add_additional_inventory_meta_field_keys()) {

            // add base item info
            $item = [
                'item_category' => $current_cat,
                'item_id' => $product->get_id(),
                'item_name' => $product->get_title(),
                'price' => wc_get_price_including_tax($product),
                'sku' => $product->get_sku(),
                'sell_on_google_quantity' => $product->get_stock_quantity(),
                'item_variant' => $product->get_parent_id() ? implode(', ', $product->get_variation_attributes()) : '',
            ];

            // add quantity
            if (is_numeric($quantity)) {
                $item['quantity'] = $quantity;
            }

            // skip certain meta keys
            $skip_meta_keys = ['ad_group', 'top_category'];

            // add additional tracking fields
            foreach ($meta_field_keys as $meta_field_key => $meta_field_key_label) {
                if (in_array($meta_field_key, $skip_meta_keys)) {
                    continue;
                }
                $item[$meta_field_key] = $product->get_meta($meta_field_key);
            }
        }

        // remove any items that have an empty value
        $item = array_filter($item, function ($value) {
            return $value !== null && $value !== "";
        });

        // return item
        return apply_filters('ud_analytics_generate_event_item', $item, $product);
    }


    /**
     * Get event items total
     */
    function get_event_items_value($event_items) {
        $total = 0;
        if (!empty($event_items)) {
            foreach ($event_items as $item) {
                // convert price to a float
                $price = floatval($item['price']);

                // get the quantity of the item
                $quantity = isset($item['quantity']) ? intval($item['quantity']) : 1;

                // calculate the subtotal for the item (price * quantity)
                $subtotal = $price * $quantity;

                // add the subtotal to the total
                $total += $subtotal;
            }
        }

        // total to 2 decimal places
        $formatted_total = number_format($total, 2);

        // return formatted total
        return apply_filters('ud_analytics_get_event_items_value', $formatted_total, $total, $event_items); // hook here used on tradefix to add vat total value
    }


    /**
     * Setup gtag ecommerce js
     *  • ud-event => gtag mapping
     */
    public function setup_gtag_ecommerce_js() {

        // setup variable to store js
        $js = '';

        // add function to add event data to datalayer
        $js .= <<<EOD

window.ud_gta = (event, vars = false) => {
    if (typeof window.dataLayer !== 'undefined') {

        console.log('ud_gta');
        console.log(event);
        console.log(vars);

        if (!vars) {
            vars = {};
        }
        if (event) {
            vars['event'] = event;
        }
        if (event == 'view_cart') {
            dataLayer.push({ ecommerce: null });
        }
        window.dataLayer.push(vars);
    }
};

const ud_ecommerce_events = {
    add_to_cart: (e, vars) => {
       dataLayer.push({ ecommerce: null });
       window.ud_gta('add_to_cart', {'ecommerce' : vars.vars.ecommerce});
    },
    remove_from_cart: (e, vars) => {
        window.ud_gta('remove_from_cart', {'ecommerce' : vars.vars.ecommerce});
    },
    update_cart: (e, vars) => {
        window.ud_gta('update_cart', {'ecommerce' : vars.vars.ecommerce});
    },
    open_basket: (e, vars) => {
        window.ud_gta('view_cart', {'ecommerce' : vars.vars.ecommerce});
    },
    view_cart: (e, vars) => {
        window.ud_gta('view_cart', {'ecommerce' : vars.vars.ecommerce});
    },
    begin_checkout: (e, vars) => {
        window.ud_gta('begin_checkout', {'ecommerce' : vars.vars.ecommerce});
    },
    view_item: (e, vars) => {
        window.ud_gta('view_item', {'ecommerce' : vars.vars.ecommerce});
    },
    view_item_list: (e, vars) => {
        window.ud_gta('view_item_list', {'ecommerce' : vars.vars.ecommerce});
    },
    purchase: (e, vars) => {
        window.ud_gta('purchase', {'ecommerce' : vars.vars.ecommerce});
    }
};

jQuery(document.body).on('ud-event', function (e, event_name, vars) {
    // console.log('track: ' + event_name);
    // console.log(vars);
    if (typeof ud_ecommerce_events[event_name] === 'function') {
        ud_ecommerce_events[event_name](e, vars);
    }
});

jQuery(document.body).trigger('ud_ecommerce_tracking_started');

EOD;

        echo $js;
    }


    /**
     * Clear gtag ecommerce array before view cart event
     */
    public function event_js_view_cart_clear_ecommerce_array($js) {
        return "\r\n dataLayer.push({ ecommerce: null }); \r\n" . $js;
    }


}

// global function
function UDAnalyticsEcommerceTracking()
{
    return UDAnalyticsEcommerceTracking::instance();
}

// run initialization
UDAnalyticsEcommerceTracking()->init();
