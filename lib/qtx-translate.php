<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class UDETTranslate {


    /**
     * Only load if qtranslate is active
     */
    public function __construct()
    {
        if (!is_plugin_active('qtranslate-xt/qtranslate.php')) {
            return;
        }
        add_action('init', [$this, 'init']);
    }


    /**
     * On class construction
     */
    public function init()
    {

        global $pagenow;

        // dont run on wp login
        if ('wp-login.php' == $pagenow) {
            return;
        }

        if (is_admin()) {

            // load additional q translate config
            add_action('i18n_admin_config', [$this, 'ud_analytics_admin_page_config']);

        } else {

            global $q_config;

            // if requesting `ud-analytics.js` disable qtranslate redirect
            if ($this->is_requesting_ud_analytics_js()) {
                add_filter('qtranslate_language_detect_redirect', '__return_false');
                qtranxf_init_language();
            } else  {

                // of q config not loaded then load it
                if (empty($q_config)) {
                    add_filter('qtranslate_language_detect_redirect', [$this, 'detect_redirect'], 10, 3);
                    qtranxf_init_language();
                }

            }

            // translate fields
            add_filter('uda_field_cookie_popup_title', [$this, 'translate_field']);
            add_filter('uda_field_cookie_popup_paragraph', [$this, 'translate_field']);
            add_filter('uda_field_understand_button_text', [$this, 'translate_field']);
            add_filter('uda_field_necessary_cookie_info', [$this, 'translate_field']);
            add_filter('uda_field_analytical_cookie_info', [$this, 'translate_field']);
            add_filter('uda_field_marketing_cookie_info', [$this, 'translate_field']);
            add_filter('uda_field_analytical_cookie_scripts', [$this, 'translate_field']);
            add_filter('uda_field_marketing_cookie_scripts', [$this, 'translate_field']);
            add_filter('ud_analytics_get_gtm_code', [$this, 'translate_field']);

            // add js to footer to listen for language change and clear cookies to indicate selection
            add_action('wp_footer', [$this, 'listen_change_lang_js']);

        }

    }


    /**
     * Detect if requesting ud-analytics.js
     */
    function is_requesting_ud_analytics_js() {
        return isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] && 'ud-analytics.js' == trim(strtok($_SERVER["REQUEST_URI"], '?'), '/');

    }


    /**
     * Load additional q translate config
     */
    function ud_analytics_admin_page_config($page_configs) {
        $page_config = [];
        $page_config['pages'] = ['admin.php' => 'page=ud-analytics'];
        $page_config['forms'] = [];
        $f = [];
        $f['form'] = ['id' => 'ud-analytics-analytic-form'];
        $f['fields'] = [];
        $fields = &$f['fields'];
        $fields[] = ['tag' => 'input', 'name' => 'gtm_number'];
        $page_config['forms'][] = $f;
        $page_configs[] = $page_config;
        $page_config = [];
        $page_config['pages'] = ['admin.php' => 'page=ud-analytics-cookies'];
        $page_config['forms'] = [];
        $f = [];
        $f['form'] = ['id' => 'ud-analytics-cookies-form'];
        $f['fields'] = [];
        $fields = &$f['fields'];
        foreach (['cookie_popup_title', 'understand_button_text'] as $tmp_field) {
            $fields[] = ['tag' => 'input', 'name' => $tmp_field];
        }
        foreach (['cookie_popup_paragraph', 'necessary_cookie_info', 'analytical_cookie_info', 'marketing_cookie_info', 'analytical_cookie_scripts', 'marketing_cookie_scripts'] as $tmp_field) {
            $fields[] = ['tag' => 'textarea', 'name' => $tmp_field];
        }
        $page_config['forms'][] = $f;
        $page_configs[] = $page_config;
        return $page_configs;
    }


    /**
     * Translate field
     */
    function translate_field($value) {
        global $q_config;
        return qtranxf_use( $q_config['language'], $value, true, false );
    }


    /**
     * Listen for language change and clear cookies to indicate selection
     */
    function listen_change_lang_js() { ?>
        <script>
            jQuery(document).on("click", ".qtranxs-lang-menu-item a", function (e) {
                e.preventDefault();
                document.cookie = 'ud_accept_analytical_cookies=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = 'ud_accept_marketing_cookies=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                window.location.href = jQuery(this).attr('href');
            });
        </script>
        <?php
    }


    /**
     * Over-ride qtx translates redirect as its using wp_redirect() too early
     */
    function detect_redirect($url_lang, $url_orig, $url_info ) {
        if ( $url_lang !== false && $url_lang != $url_orig ) {
            header('Location: '.$url_lang);
            exit();
        }
    }


}

new UDETTranslate();
