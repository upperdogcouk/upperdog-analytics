window.ud_cookies_expiry_days = 30;

window.uda_set_cookie = function(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
};

window.uda_get_cookie = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
};

window.click_ud_manage_cookies_button = function (){
    document.getElementById("c-cat").classList.toggle("active");
    document.getElementById("js-cookies-manage").classList.toggle("open");
    document.getElementById("cpu").classList.toggle("cpu-overflow");
}

window.click_ud_accept_analytical_cookies_button = function (force = false){
    if (force || document.getElementById('ud_accept_analytical_cookies_checkbox').checked) {
        window.uda_set_cookie('ud_accept_analytical_cookies', 1, window.ud_cookies_expiry_days);
        document.body.dispatchEvent(new CustomEvent("do_ud_analytical_cookies_js", {
            detail: {},
            bubbles: true,
            cancelable: true,
            composed: false,
        }));
    }
    document.body.classList.remove('cpu-open');
}

window.click_ud_accept_marketing_cookies_button = function (force = false){
    if (force || document.getElementById('ud_accept_marketing_cookies_checkbox').checked) {
        window.uda_set_cookie('ud_accept_marketing_cookies', 1, window.ud_cookies_expiry_days);
        document.body.dispatchEvent(new CustomEvent("do_ud_marketing_cookies_js", {
            detail: {},
            bubbles: true,
            cancelable: true,
            composed: false,
        }));
    }
    document.body.classList.remove('cpu-open');
}

window.click_ud_save_cookies_button = function (){
    window.uda_set_cookie('ud_accept_marketing_cookies', (document.getElementById('ud_accept_marketing_cookies_checkbox').checked ? 1 : false), window.ud_cookies_expiry_days);
    window.uda_set_cookie('ud_accept_analytical_cookies', (document.getElementById('ud_accept_analytical_cookies_checkbox').checked ? 1 : false), window.ud_cookies_expiry_days);
    location.reload();
}

window.click_ud_accept_all_cookies_button = function (){
    window.uda_set_cookie('ud_accept_marketing_cookies', 1, window.ud_cookies_expiry_days);
    window.uda_set_cookie('ud_accept_analytical_cookies', 1, window.ud_cookies_expiry_days);
    window.click_ud_accept_analytical_cookies_button(true);
    window.click_ud_accept_marketing_cookies_button(true);
    document.getElementById('cpu-wrapper').classList.add('pop-up-hidden');
    document.body.classList.remove('cpu-open');
}

window.click_ud_reject_all_cookies_button = function (){
    window.uda_set_cookie('ud_accept_marketing_cookies', false, window.ud_cookies_expiry_days);
    window.uda_set_cookie('ud_accept_analytical_cookies', false, window.ud_cookies_expiry_days);
    location.reload();
}

window.click_ud_accept_essential_cookies_button = function (){
    window.uda_set_cookie('ud_accept_marketing_cookies', false, window.ud_cookies_expiry_days);
    window.uda_set_cookie('ud_accept_analytical_cookies', false, window.ud_cookies_expiry_days);
    document.getElementById('cpu-wrapper').classList.add('pop-up-hidden');
    document.body.classList.remove('cpu-open');
}

window.click_cookie_consent_config_button = function (){
    document.getElementById('cpu-wrapper').classList.remove('pop-up-hidden');
    document.body.classList.add('cpu-open');
}
