<?php
/**
 * @package Upperdog_Analytics
 * @version 2.4.5
 */
/*
Plugin Name: Upperdog Analytics
Plugin URI:
Description: A plugin for Upperdog clients to provide a concise cookie consent pop-up, and event tracking.
Author: Upperdog
Version: 2.5.0
Author URI: http://upperdog.co.uk/
*/

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

// define const for analytics file location
define('UPPERDOG_ANALYTICS_FILE', __FILE__);

if ( ! function_exists( 'is_plugin_active' ) ) {
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
}

require_once 'lib/qtx-translate.php';
require_once 'lib/cookie-consent.php';
require_once 'lib/cookie-free-tracking.php';
require_once 'lib/ecommerce-tracking.php';
require_once 'lib/event-tracking.php';


// update checker library
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://plugins.upperdog.co.uk/upperdog-analytics/details.json',
    __FILE__,
    'upperdog-analytics'
);


class UDAnalytics {

    /**
     * The single instance of the class.
     */
    private $fields = [
        'gtm_number' => '',
        'load_ecommerce_tracking' => false,
        'tracking_product_meta_keys' => 'gtin
mpn
cost_of_good_sold
expiry_date
custom_label_0
custom_label_1
custom_label_2
custom_label_3
custom_label_4
g_product_type',
    ];


    /**
     * The single instance of the class.
     */
    protected static $_instance = null;


    /**
     * Ensures only one instance of `UDAnalytics` is loaded or can be loaded.
     */
    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    /**
     * On class initialization
     */
    public function init()
    {

        // on install
        register_activation_hook(UPPERDOG_ANALYTICS_FILE, [UDAnalytics(), 'on_plugin_install']);

        // load default field data into wp options
        $this->load_option_settings();

        // hook listeners
        $this->listeners();
    }


    /**
     * On plugin install
     */
    public function on_plugin_install()
    {

        // do specific plugin install action
        do_action('upperdog_analytics_plugin_install');
    }


    /**
     * Listen for event hooks
     */
    function listeners() {

        // add `ud analytics` admin menu item
        add_action('admin_menu', [$this, 'admin_menu']);

        // custom xml url
        add_filter('init', [$this, 'setup_js_url_rule']);

        // capture js url and pass query var to indicate js request
        add_filter('query_vars', [$this, 'query_vars']);

        // setup default wp option fields
        add_action('upperdog_analytics_plugin_install', [$this, 'setup_fields']);

        // process/save fields from cms
        add_action('upperdog_analytics_process_tab_ud-analytics', [$this, 'process_analytics_setting']);

        // parse request and output dynamic js file
        add_filter('parse_request', [$this, 'parse_request']);

        // lazy load upperdog analytics js file ud-analytics.js
        add_action('wp_footer', [$this, 'lazy_load_js']);
    }


    /**
     * Setup dynamic js file
     */
    function setup_js_url_rule()
    {
        add_rewrite_rule('ud-analytics.js$', 'index.php?ud-analytics=1', 'top');
    }


    /**
     * Capture url and pass query var to indicate js request
     */
    function query_vars($query_vars)
    {
        $query_vars[] = 'ud-analytics';
        return $query_vars;
    }


    /**
     * Output facebook formatted product xml
     */
    function parse_request(&$wp)
    {
        if (array_key_exists('ud-analytics', $wp->query_vars)) {
            header_remove('Content-type');
            header('Content-type: text/javascript');
            $this->output_ud_analytics_js();
            exit();
        }
    }


    /**
     * Load default field data into wp options
     */
    function load_option_settings()
    {
        if ($upperdog_analytics_settings = get_option("upperdog_analytics_settings")) {
            foreach ($upperdog_analytics_settings as $k => $v) {
                $this->fields[$k] = $v;
            }
        }
    }


    /**
     * Get google analytics ga number
     */
    function get_gtm_code()
    {
        return apply_filters('ud_analytics_get_gtm_code', $this->fields['gtm_number']);
    }


    /**
     * Get product meta keys that we would like to include in the gta tracking
     */
    function get_tracking_product_meta_keys()
    {
        return apply_filters('ud_analytics_get_tracking_product_meta_keys', explode("\r\n", trim($this->fields['tracking_product_meta_keys'])));
    }


    /**
     * Return setting to indicate if the user wants to load ecommerce tracking
     */
    function is_tracking_ecommerce()
    {
        return !empty(get_option("upperdog_analytics_settings")['load_ecommerce_tracking']);
    }


    /**
     * Get upperdog analytics plugin url
     */
    function uri($path)
    {
        return plugins_url('upperdog-analytics/' . $path);
    }


    /**
     * Process/save fields from cms
     */
    function process_analytics_setting()
    {
        if (!isset($_POST['save'])) {
            return;
        }
        $fields = [];
        foreach ( $this->fields as $field_k => $field) {
            $fields[$field_k] = isset($_POST[$field_k]) ? $_POST[$field_k] : '';
        }

        // update settings
        update_option("upperdog_analytics_settings", $fields);

        // return response to hook
        add_action('upperdog_analytics_process_tab_response', function () { ?>
            <div class="notice notice-success is-dismissible">
                <p>Upperdog analytics settings saved</p>
            </div>
            <?php
        });

    }


    /**
     * Setup default wp option fields
     */
    function setup_fields()
    {
        if (!get_option("upperdog_analytics_settings")) {
            update_option("upperdog_analytics_settings", $this->fields);
        }
    }


    /**
     * Menu links variable
     */
    private $links = [
        0 =>  [
            'label'     => 'Cookies',
            'slug'      => 'cookies',
        ]
    ];


    /**
     * Root slug
     */
    private $root_slug = 'ud-analytics';


    /**
     * Admin menu
     */
    function admin_menu()
    {

        // create new root admin page
        add_menu_page('UD Analytics', 'UD Analytics', 'edit_theme_options', $this->root_slug, [$this, 'admin_page_html'], 'dashicons-chart-area', 100);

        // add sub admin pages
        foreach ($this->links as $link_i => $link) {
            add_submenu_page($this->root_slug, $link['label'], $link['label'], 'edit_theme_options', $this->root_slug . '-' . $link['slug'], [$this, 'admin_page_html']);
        }
    }


    /**
     * Render admin page html
     */
    function admin_page_html()
    {

        // only render for admins
        if (!is_admin()) {
            return;
        }

        // render admin page
        $this->render_admin_page();

    }


    /**
     * Render current navigated admin page
     */
    function render_admin_page()
    {

        // get current tab
        $tab = $this->get_current_tab_slug();

        // initial view
        $view = 'cms-view/' . $tab . '.php';

        // perform action
        do_action('upperdog_analytics_process_tab_' . $tab);

        // wrap in woocommerce wrapper
        echo '<div class="wrap woocommerce">';

        // output response from tab process
        do_action('upperdog_analytics_process_tab_response');

        // load view
        include($view);

        echo '</div>';

    }


    /**
     * Get current tab slug
     */
    function get_current_tab_slug()
    {
        return (!empty($_GET['page']) ? str_replace($this->root_slug . '-','',$_GET['page']) : $this->links[0]['slug']);
    }


    /**
     * Get current tab url
     */
    function get_current_tab_url()
    {
        $page = $this->root_slug == $this->get_current_tab_slug() ? $this->get_current_tab_slug() : $this->root_slug . '-' . $this->get_current_tab_slug();
        return get_admin_url($blog_id = null, 'admin.php?page=' . $page);
    }


    /**
     * Lazy load upperdog analytics js file ud-analytics.js
     */
    function lazy_load_js()
    {

        // js to lazy load
        $js = '
        (function() {
            var uda = document.createElement(\'script\'); uda.type = \'text/javascript\'; uda.async = true;
            var uda_js_url = \'' . base64_encode($this->get_ud_analytics_js_uri()) . '\';
            uda.src = atob(uda_js_url) + \'?\' + String(Math.random()).replace(".", "");
            var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(uda, s);
        })();';

        // load 0.5 seconds after document has fully loaded
        $html = '<script> window.addEventListener("load", function(){ setTimeout(function(){ ' . $js . ' }, 500); }); </script>';

        // output html
        echo $html;
    }


    /**
     * Returns the url of the dynamic external analytics js
     */
    function get_ud_analytics_js_uri()
    {
        return get_home_url(null, 'ud-analytics.js');
    }


    /**
     * Hook that output's inline analytics js
     */
    function output_ud_analytics_js()
    {
        do_action('upperdog_analytics_output_ud_analytics_js');
    }


}


/**
 * Public function instantiating wish list class
 */
function UDAnalytics() {
    return UDAnalytics::instance();
}


/**
 * Initializing functions & listeners
 */
UDAnalytics()->init();
