<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$fields = get_option("upperdog_analytics_cookie_consent");


$toggle_colour = '#' . UDAnalyticsCookieConsent()->get_field('cookie_toggle_colour');
$title_colour = '#' . UDAnalyticsCookieConsent()->get_field('cookie_title_colour');
$background_colour = UDAnalyticsCookieConsent()->get_field('cookie_background_colour');
$show_analytical_cookies_toggler = UDAnalyticsCookieConsent()->show_analytical_cookies_toggler();
$show_marketing_cookies_toggler = UDAnalyticsCookieConsent()->show_marketing_cookies_toggler();
$is_cookie_consent_not_required = UDAnalyticsCookieConsent()->is_cookie_consent_not_required();
?>

<style>
    .cpu {
        --toggleColour: <?php echo ($toggle_colour ? $toggle_colour : '#2196f3');?>;
        --titleColour: <?php echo ($title_colour ? $title_colour : '#FFF');?>;
        <?php if($background_colour == 'light') { ?>

            --background: #FFF;
            --textColour: #000;
            --titleColour: #000;
            --toggleOffColour: #a4a4a4;
            --hrColour: #c1c1c1;

            --acceptButtonBg: #000;
            --acceptButtonText: #FFF;
            --acceptButtonBorder: #000;

            --rejectButtonBg: transparent;
            --rejectButtonText: #000;
            --rejectButtonBorder: #000;

            --preferencesButtonText: #000;
            --boxShadow: 0px 0px 3px 0px #ccc;

        <?php } ?>
    }
</style>
<div class="cpu-wrapper" id="cpu-wrapper">
    <div class="cpu" id="cpu">
        <h2 class="cpu-title"><?=stripslashes($fields['cookie_popup_title'])?></h2>
        <p class="cpu-para"><?=stripslashes($fields['cookie_popup_paragraph'])?></p>

        <div class="cpu-buttons">
        <?php if (!$is_cookie_consent_not_required) : ?>
            <button class="js-cookie-accept" onclick="window.click_ud_accept_all_cookies_button();"><?=apply_filters( 'ud_analytics_accept_all', 'Accept All');?></button>
            <?php // <button class="js-cookie-reject" onclick="window.click_ud_reject_all_cookies_button();"><?=apply_filters( 'ud_analytics_reject_all', 'Reject All');?> <?php //</button> ;?>
            <button class="js-cookies-manage" id="js-cookies-manage" onclick="window.click_ud_manage_cookies_button();"><?=apply_filters( 'ud_analytics_preferences', 'Manage Cookies');?></button>
        <?php else: ?>
            <button class="js-cookie-accept" onclick="window.click_ud_accept_essential_cookies_button();"><?php echo stripslashes($fields['understand_button_text']); ?></button>
        <?php endif; ?>
        </div>

        <div class="c-cat" id="c-cat">
            <hr>

            <div class="c-cat-header">
                <h3><?=apply_filters( 'ud_analytics_necessary_cookies', 'Necessary Cookies');?></h3>
                <?=apply_filters( 'ud_analytics_required', 'Required');?>
            </div>


            <p><?=stripslashes($fields['necessary_cookie_info'])?></p>

            <?php if (!$is_cookie_consent_not_required) : ?>
                <?php if ($show_analytical_cookies_toggler) : ?>
                <hr>
                <div class="c-cat-header">
                    <h3><?=apply_filters( 'ud_analytics_analytical_cookies', 'Analytical Cookies');?></h3>
                    <label class="c-cat-toggle">
                        <input type="checkbox" name="analytical-cookies" id="ud_accept_analytical_cookies_checkbox" onclick="window.click_ud_accept_analytical_cookies_button()" <?=(UDAnalyticsCookieConsent()->has_accepted_analytical_cookies() ? ' checked' : '');?>>
                        <div class="toggle"></div>
                    </label>
                </div>

                <p><?=stripslashes($fields['analytical_cookie_info'])?></p>

                <?php endif; ?>

                <?php if ($show_marketing_cookies_toggler) : ?>
                <hr>
                <div class="c-cat-header">
                    <h3><?=apply_filters( 'ud_analytics_marketing_cookies', 'Marketing Cookies');?></h3>
                    <label class="c-cat-toggle">
                        <input type="checkbox" name="marketing-cookies" id="ud_accept_marketing_cookies_checkbox" onclick="window.click_ud_accept_marketing_cookies_button()" <?=(UDAnalyticsCookieConsent()->has_accepted_marketing_cookies() ? ' checked' : '');?>>
                        <div class="toggle"></div>
                    </label>
                </div>

                <p><?=stripslashes($fields['marketing_cookie_info'])?></p>
                <?php endif; ?>

            <button class="js-cookie-save" onclick="window.click_ud_save_cookies_button();"><?=apply_filters( 'ud_analytics_save_preferences', 'Save Preferences');?></button>

            <?php endif; ?>

        </div>
    </div>
</div>
