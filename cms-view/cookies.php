<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$fields = get_option("upperdog_analytics_cookie_consent");
?>
<div class="wrap">
    <h2>Cookies Popup</h2>

    <form name="ud-analytics-cookies-form" id="ud-analytics-cookies-form" action="<?php echo UDAnalytics()->get_current_tab_url(); ?>" method="post">

        <h2>General Cookie Information</h2>
        <table class="form-table" role="presentation">
            <tbody>
            <tr>
                <th scope="row">
                    <label for="cookie_popup_title">Cookie popup title</label>
                </th>
                <td>
                    <input name="cookie_popup_title" type="text" id="cookie_popup_title" value="<?=apply_filters('ud_analytics_cookie_popup_title', stripslashes($fields['cookie_popup_title']))?>" class="regular-text">
                    <small>Leave blank if you want to disable the cookie popup notification.</small>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cookie_popup_paragraph">Cookie popup paragraph</label>
                </th>
                <td>
                    <textarea name="cookie_popup_paragraph" id="cookie_popup_paragraph" class="large-text" rows="3"><?=apply_filters('ud_analytics_cookie_popup_paragraph', stripslashes($fields['cookie_popup_paragraph']))?></textarea>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cookie_toggle_colour">Toggle colour</label>
                </th>
                <td>
                    # <input name="cookie_toggle_colour" type="text" id="cookie_toggle_colour" value="<?=apply_filters('ud_analytics_cookie_toggle_colour', $fields['cookie_toggle_colour'])?>" placeholder="2196f3" class="regular-text" style="width: 100px">
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cookie_title_colour">Title colour</label>
                </th>
                <td>
                    # <input name="cookie_title_colour" type="text" id="cookie_title_colour" value="<?=apply_filters('ud_analytics_cookie_title_colour', $fields['cookie_title_colour'])?>" placeholder="ffffff" class="regular-text" style="width: 100px">
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="cookie_background_colour">Background colour</label>
                </th>
                <td>
                    <select name="cookie_background_colour" id="cookie_background_colour">
                        <option value="dark" <?=('dark' == $fields['cookie_background_colour'] ? 'selected' : '')?>>Dark</option>
                        <option value="light" <?=('light' == $fields['cookie_background_colour'] ? 'selected' : '')?>>Light</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="understand_button_text">Understand button text</label>
                </th>
                <td>
                    <input name="understand_button_text" type="text" id="understand_button_text" value="<?=apply_filters('ud_analytics_understand_button_text', stripslashes($fields['understand_button_text']))?>" placeholder="I Understand" class="regular-text">
                </td>
            </tr>
            </tbody>
        </table>

        <h2>Necessary Cookie</h2>
        <table class="form-table" role="presentation">
            <tbody>
            <tr>
                <th scope="row">
                    <label for="necessary_cookie_info">Explainer paragraph</label>
                </th>
                <td>
                    <textarea name="necessary_cookie_info" id="necessary_cookie_info" class="large-text" rows="2"><?=apply_filters('ud_analytics_necessary_cookie_info', stripslashes($fields['necessary_cookie_info']))?></textarea>
                </td>
            </tr>
            </tbody>
        </table>

        <h2>Analytical Cookies</h2>
        <table class="form-table" role="presentation">
            <tbody>
            <tr>
                <th scope="row">
                    <label for="analytical_cookie_info">Explainer paragraph</label>
                </th>
                <td>
                    <textarea name="analytical_cookie_info" id="analytical_cookie_info" class="large-text" rows="2"><?=apply_filters('ud_analytics_analytical_cookie_info', stripslashes($fields['analytical_cookie_info']))?></textarea>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="analytical_cookie_scripts">Scripts</label>
                </th>
                <td>
                    <textarea style="font-family: 'Courier New', monospace;"  name="analytical_cookie_scripts" id="analytical_cookie_scripts" class="large-text" rows="12"><?=apply_filters('ud_analytics_analytical_cookie_scripts', stripslashes($fields['analytical_cookie_scripts']))?></textarea>
                </td>
            </tr>
            </tbody>
        </table>

        <h2>Marketing Cookies</h2>
        <table class="form-table" role="presentation">
            <tbody>
            <tr>
                <th scope="row">
                    <label for="marketing_cookie_info">Explainer paragraph</label>
                </th>
                <td>
                    <textarea name="marketing_cookie_info" id="marketing_cookie_info" class="large-text" rows="2"><?=apply_filters('ud_analytics_marketing_cookie_info', stripslashes($fields['marketing_cookie_info']))?></textarea>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="marketing_cookie_scripts">Scripts</label>
                </th>
                <td>
                    <textarea style="font-family: 'Courier New', monospace;" name="marketing_cookie_scripts" id="marketing_cookie_scripts" class="large-text" rows="12"><?=apply_filters('ud_analytics_marketing_cookie_scripts', stripslashes($fields['marketing_cookie_scripts']))?></textarea>
                </td>
            </tr>
            </tbody>
        </table>

        <input name="save" type="hidden" value="1">
        <input type="submit" value="Save" class="button button-primary button-large" id="save" name="save">
    </form>
</div>

<?php do_action('ud_analytics_after_cookies_panel'); ?>
