<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$fields = get_option("upperdog_analytics_settings");
?>
<div class="wrap">
    <h2>Google Analytic Settings</h2>
    <div style="padding: 1em; background: #de3030; color: #FFF;">
        <h2 style="font-size: 1.5em; color: #FFF; margin-top: 0;">Reminder</h2>
        <p style="font-size: 1.25em; margin: 0;">Add Cookie Exclusions to Varnish Application settings in Cloudways for the following Cookies:</p>

        <pre>ud_accept_analytical_cookies</pre>
        <pre>ud_accept_marketing_cookies</pre>
    </div>

    <form name="ud-analytics-analytic-form" id="ud-analytics-analytic-form" action="<?php echo UDAnalytics()->get_current_tab_url(); ?>" method="post">
        <table class="form-table" role="presentation">
            <tbody>
            <tr>
                <th scope="row">
                    <label for="gtm_number">GTM Number</label>
                    <p>i.e. GTM-XXXXXX</p>
                </th>
                <td>
                    <input name="gtm_number" type="text" id="gtm_number" placeholder="GTM-XXXXXX" value="<?=apply_filters('ud_analytics_gtm_number', $fields['gtm_number'])?>" class="regular-text">
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="load_ecommerce_tracking">Load Ecommerce Tracking Code</label>
                </th>
                <td>
                    <input name="load_ecommerce_tracking" type="checkbox" id="load_ecommerce_tracking" value="1" <?=(!empty($fields['load_ecommerce_tracking']) ? 'checked' : '')?>>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="tracking_product_meta_keys" style="display: block">Ecommerce Tracking Product Meta</label>
                    <small style="font-weight: normal">Enter any custom meta fields you would like to be included in the GTAG item data, one per line.</small>
                </th>
                <td>
                    <textarea name="tracking_product_meta_keys" id="tracking_product_meta_keys" style="width: 100%; min-height: 300px;"><?=apply_filters('tracking_product_meta_keys', (empty($fields['tracking_product_meta_keys']) ? '' :  $fields['tracking_product_meta_keys']))?></textarea>
                </td>
            </tr>
            </tbody>
        </table>



        <input name="save" type="hidden" value="1">
        <p class="submit">
            <input type="submit" value="Save" class="button button-primary button-large" id="save" name="save">
        </p>
    </form>
</div>

<?php do_action('ud_analytics_after_analytic_panel'); ?>

<script>

    let $ = jQuery;

    function addNewEventTrackingRule() {
        let new_rule_tr_html = '<tr><th class="check-column"><span class="delete-rule">[&times;]</span></th><td><input name="rule_i[]" type="hidden" value=""><input name="selector[]" type="text" required style="font-family: monospace;" value=""></td><td><input name="action[]" required type="text" value=""></td><td><input name="event_category[]" required type="text" value=""></td><td><input name="event_action[]" required type="text" value=""></td><td><input name="event_label[]" type="text" value=""></td></tr>';
        $(".event-tracking-rules-body").append(new_rule_tr_html);
    }

    $(document.body).on('click', '#add-new-event-tracking-rule', function () {
        addNewEventTrackingRule();
    });

    $(document.body).on('click', '.delete-rule', function () {
        $(this).closest('tr').remove();
    });

</script>
